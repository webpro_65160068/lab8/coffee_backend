import { Order } from "src/orders/entities/order.entity"
import { Role } from "src/roles/entities/role.entity"
import { Column, CreateDateColumn, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm"


@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    email: string

    @Column()
    password: string

    @Column()
    gender: string

    @CreateDateColumn()
    created: Date

    @UpdateDateColumn()
    updated: Date

    @ManyToMany(() => Role, (role) => role.users, { cascade: true})
    @JoinTable()
    roles: Role[];

    @OneToMany(() => Order, (order) => order.user)
    orders: Order[];

}
